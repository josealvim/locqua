\documentclass{article}

\usepackage{quiver}
\usepackage{style}
\usepackage{hyperref, nameref}
\usepackage{bussproofs}

\newcommand\putintoc[2] {\addcontentsline{toc}{subsection}{\textbf{#1.} #2}}
\newcommand\deftotoc[1] {\putintoc{Def}{#1}}
\newcommand\thmtotoc[1] {\putintoc{Thm}{#1}}

\begin{document}
	\title {\bf
		Local Character of Commutative Divisible Integral Quantales 
	}

	\author{}

	\maketitle

	\begin{abstract}
		We are interested in certain symmetric monoidal closed 
		categories with finite limites and all colimits with 
		co-continuous monoidal products. This property is exemplified
		by commuative integral quantales with a condition called 
		“divisibility” -- which warrants their study as a structure in
		itself as to guide our study of those generic categories. This 
		work is the collection of our knowledge on those quantales.
	\end{abstract}	

	\tableofcontents
	\pagebreak

	\section{Preliminaries}
	\begin{definition}[Quantale]
		\deftotoc {Quantale}
		A quantale is a complete partial order with a monoidal 
		structure which distributes over suprema (both to the right and
		to the left). It has, therefore, a bounded lattice structure as
		well as a monoidal one. We shall denote a quantale with:
		\[
			\left\langle 
				Q, \leq, \wedge, \vee, \top, \bot, \tensor, I
			\right\rangle
		\]
		most of that data is redundant, of course, but this is more so
		to establish notation.
	\end{definition}

	\begin{definition}[Integrality, Commutativity \textit{\&} Divisibility]
		\deftotoc {Integrality, Commutativity \textit{\&} Divisibility}$ $
		\begin{enumerate}
			\item 
			A quantale is said to be \emph{integral} 
			(or \emph{semi-cartesian}) when the monoidal unit is 
			the lattice's maximum; 

			\item 
			\emph{commutative} when the monoid is, itself, 
			commutative;

			\item 
			\emph{divisible} or to have the 
			\emph{divisibility roperty} when:
			\[
				\forall x : \forall y \leq x : \exists z :
				y = x \tensor z
			\]
			there is a difference between left and right 
			divisibility, but we will use commutativity soon, so 
			it won't matter.
		\end{enumerate}
	\end{definition}

	\begin{definition}[Residue]
		\deftotoc {Residue}
		In a \emph{commutative} quantale, it's possible to define a 
		\emph{residue} via an adjunction of the monoidal product -- in
		general there are two such constructions in non-commutative 
		quantales, as there are two positions one can multiply things
		and their order matters.
		\[
			(a \residue b) = 
			\sup\setbuilder{
				\gamma
			}{a\tensor\gamma\leq b}
		\]
		This is expressed by 
		\begin{prooftree}
			\AxiomC{\(\gamma\leq a\residue b\)}
			\doubleLine
			\UnaryInfC{\(a\tensor\gamma\leq b\)}
		\end{prooftree}
		and the fact that you can reach one from the other can be 
		readily verified. This description of \(\tensor\) and 
		\(\residue\) make it more aparent that 
		\[
			(a\tensor-) \adjoint (a\residue-)
		\]
		and thus the former preserves colimites (suprema) and the 
		latter preserves finite infima (\(\wedge\)).
	\end{definition}

	\begin{lemma}
	\label{lemma:integrality}
		Being integral is equivalent to the following:  
		\[
			x\tensor y \leq x \land y
		\]
	\end{lemma}
	\begin{proof}
		\begin{align*}
			 x = 
			 x \tensor\top =
			 x \tensor(y \vee\top) = 
			(x \tensor y)\vee(x \tensor \top) = 
			(x \tensor y)\vee x 
		\end{align*}
		consequently, \(x\tensor y\leq x\); and similarly for \(y\). So
		therefore, \(x\tensor y \leq x\wedge y\); conversely, it is 
		always the case that 
		\[
			x\tensor\top = x\tensor(\top\vee I) = (x\tensor\top)\vee x
		\]
		but if \(x\tensor y\leq x\wedge y\), then \(x\tensor\top\leq x\);
		and thus \(x\tensor\top=x\); and similarly for the inverse
	\end{proof}

	\begin{lemma}
	\label{lemma:divisibilty}
		In any quantale we have 
		\[
			x\tensor [x\residue(x\tensor y)] = x\tensor y
		\]
	\end{lemma}
	\begin{proof}
		\begin{align*}
			\tag{by definition}
			x \residue (x\tensor y)
			\phantom{[} 
			&=
			\phantom{x\tensor}\hspace*{2.5pt}
			\sup\setbuilder{
				z
			}{
				x\tensor z \leq 
				x\tensor y
			}\\ 
		%
			\tag{equality}
			x\tensor [x \residue (x\tensor y)]
			&=
			x\tensor 
			\sup\setbuilder{
				z
			}{
				x\tensor z \leq 
				x\tensor y
			}\\ 
		%
			\tag{distributivity}
			x\tensor [x \residue (x\tensor y)]
			&=
			\sup\setbuilder{
				x\tensor z
			}{
				x\tensor z \leq 
				x\tensor y
			}
		\end{align*}
		so, evidently, \(x\tensor [x \residue (x\tensor y)]\) is less
		than \(x\tensor y\); this much always happens. The converse is
		more interesting, using the fact that tensoring is increasing:
		\begin{prooftree}
			\AxiomC
				{\(x\tensor y\leq x\tensor y\)}
			\doubleLine
			\UnaryInfC
				{\(y\leq x\residue(x\tensor y)\)}
			\UnaryInfC
				{\(x\tensor y\leq x\tensor[x\residue(x\tensor y)]\)}
		\end{prooftree}
	\end{proof}

	\begin{corollary}
	\label{corollary:divisible}
		When the above lemma is combined with divisibility, it yields:
		that whenever \(y\leq x\),
		\[
			x\tensor [x\residue y] = y
		\]
		which therefore makes canonical the seemingly arbitary choice 
		of \(z\) in the divisibility axiom:
		\[
			\forall x: \forall y \leq x: x\tensor[x\residue y] = y
		\]
		\qed
	\end{corollary}
	
	\section{Local Character}
	The term “locally X” in category theory is usually used as a shorthand
	to say “the comma categories are X” with some possible “basis change” 
	coherence properties implied. We asked ourselves: how can quantales be
	locally quantales? In the sense that a given quantale can be seen as a 
	category whose arrows are \((a,b)\in{\,\leq}\); which is to say, the 
	there is at most one arrow between any two objects and at least one 
	arrow should the domain be \(\leq\) than the codomain.

	The comma categories of a quantale \(Q\), are -- therefore -- the 
	sublattices \(\overset\leftarrow x\), which we normally denote by 
	\(\commacat Q x\). This is naturally a complete lattice as well, as 
	finite meets of things smaller than \(x\) are, also, smaller than \(x\)
	-- and so are suprema of things smaller than \(x\).

	The question becomes to find a construction of a quantale on that 
	lattice. We could not see how to do it in general, and it's somewhat 
	hard to believe that it could be done naturally. After that, we should
	hopefully find closed forms of the quantalic local operations, and a 
	base-change quantale morphism.

	Henceforth, all the quantales we deal with are assumed to be 
	commutative, divisible and integral. We shall define a binary operation
	on \(\commacat Q x\) which makes it a quantale, with a nice expresion 
	for its residue, a simple quantale morphism for basis change, and such
	that \(\commacat Q x\) is commutative, disivible, and integral.

	\begin{definition}[Local Tensor]
		\deftotoc {Local Tensor}
		We shall define a binary operation on \(\commacat Q x\) which 
		we will verify makes it a quantale. Take \(a, b\leq x\) 
		\[
			 x
			\tensor 
			a\tensor_x b = 
			(x\residue a)
			\tensor
			(x\residue b)
		\]
	\end{definition}

	\begin {theorem} 
		\(\tensor_x\) makes \(\commacat Q x\) a CID Quantale
	\end{theorem}
	
	\begin{proof}
		\thmtotoc{\(\tensor_x\) makes \(\commacat Q x\) a CID Quantale}
		We must prove that \(\tensor_x\) is associative, distributes
		over suprema, and it has a unite. Furthermore we must prove 
		that the resulting quantale is CID (commutative, integral and 
		divisible).
		\paragraph{Associativity}
		asdf
		\begin{align*}
				(a\tensor_x b)\tensor_x c 
			&=	 x\tensor
				(x\residue
					[
				x\tensor(x\residue a)\tensor(x\residue b)
					]
				) \tensor
				(x\residue c)
			\\&=	 x\tensor
				(x\residue a)
				\tensor
				(x\residue b)
				\tensor
				(x\residue c)
		\end{align*}
		\begin{align*}
				 a\tensor_x (b\tensor_x c)
			&=	 x
				\tensor
				(x\residue a)
				\tensor
				(x\residue 
					[
				 x
				\tensor
				(x\residue b)
				\tensor
				(x\residue c)
					]
				)
			\\
			\tag{{\tiny Commutativity}}
			&=
				(x\residue a)
				\tensor
				 x
				\tensor
				(x\residue 
					[
				 x
				\tensor
				(x\residue b)
				\tensor
				(x\residue c)
					]
				)
			\\
			&=
				(x\residue a)
				\tensor
				 x
				\tensor
				(x\residue b)
				\tensor
				(x\residue c)
			\\
			\tag{{\tiny Commutativity}}
			&=
				 x
				\tensor
				(x\residue a)
				\tensor
				(x\residue b)
				\tensor
				(x\residue c)
		\end{align*}
		It is a bit sad that we seemingly need to use commutativity in
		the proof.

		\paragraph{Suprema Distribution}
		Take \(\setbuilder{a_i}{i\in I}\), with \(y, a_i\leq x\); 
		notice that 
		\[
			\bigvee_{i\in I}
				a_i 
			=
			x\tensor\bigvee_{i\in I}
				(x\residue a_i)
		\]
		a result of corollary \ref{corollary:divisible}, and therefore,
		by the same corollary,
		\[
			x \tensor
			\left(x\residue \bigvee a_i\right)
			= 
			\bigvee_{i\in I} a_i
		\]
		thus, the expression below can be rewritten as the right hand 
		side of the following equation:
		\begin{align*}
			\left(\bigvee_{i\in I} a_i\right)\tensor_x y 
			  &=
			  \left(\bigvee_{i\in I} a_i\right)\tensor(x\residue y)
			\\&=
			  \left(\bigvee_{i\in I} a_i\tensor (x\residue y)\right)
			\\&=
			  \bigvee_{i\in I} 
				x\tensor(x\residue a)\tensor(x\residue y)
			\\&=
			  \bigvee_{i\in I} 
				a_i \tensor_x y
		\end{align*}
		
		\begin{align*}
			y\tensor_x\bigvee_{i\in I} a_i
			&=	x\tensor 
				(x\residue y) 
				\tensor 
				\left(x\residue\bigvee_{i\in I} a_i\right)
			\\&=	(x\residue y)
				\tensor
				x
				\tensor
				\left(x\residue\bigvee_{i\in I} a_i\right)
			\tag{Commutativity}
			\\&=	(x\residue y)
				\tensor
				\bigvee_{i\in I} a_i
			\\&=	\bigvee_{i\in I} (x\residue y)\tensor a_i
			\\&=	\bigvee_{i\in I}
					(x\residue y)
					\tensor 
					x
					\tensor
					(x\residue a_i)
			\\&=	\bigvee_{i\in I}
					x
					\tensor 
					(x\residue y)
					\tensor
					(x\residue a_i)
			\tag{Commutativity}
			\\&=	\bigvee_{i\in I} y\tensor_x a_i
		\end{align*}
		Since the order on \(\commacat Q x\) is compatible with that on
		\(Q\), the suprema are the same, so we actually proved what we 
		wanted.

		\paragraph{Commutativity}
		Until now, we've been marking where we used commutativity, but 
		we will begin to omit those mentions from now on.

		\[
			a\tensor_x b = 
			[x\tensor(x\residue a)\tensor(x\residue b)] =
			[x\tensor(x\residue b)\tensor(x\residue a)] =
			b\tensor_x a
		\]
		
		\paragraph{Unit and Integrality}
		We shall prove both at once, as it is a simple verification.
		First, the top element in \(\commacat Q x\) is, obviously, 
		\(x\). As it is the greatest element lesser than itself. 
		\[
			y \tensor_x x = 
			x \tensor (x\residue y)\tensor(x \residue x)
		\]
		evidently, \(x\residue x\) is \(\top\) and \(\top\) is the 
		unit for \(\tensor\) so we are left with:
		\[
			y\tensor_x x = 
			x\tensor(x\residue y) =
			y
		\]
		commutativity yields nets us the same result for tensoring 
		on the left side of \(y\) instead of on the right.

		\paragraph{Divisibility}
		So far we've constructed an (integral and commutative) quantale 
		over \(\commacat Q x\), all that remains is to show it is
		divisible. For that purpose, take \(b \leq a \leq x\), we must
		show there is some \(z\leq x\) for which \(b = a\tensor_x z\).
		A candidate was \(x\tensor(a\residue b)\), which thanks to 
		integrality, is less than \(x\)
		\begin{align*}
			a\tensor_x (x\tensor(a\residue b)) &= 
			x
			\tensor 
			(x\residue a)
			\tensor
			(x\residue (x\tensor(a\residue b)))
			\\&=
			(x\residue a)
			\tensor
			x
			\tensor 
			(x\residue (x\tensor(a\residue b)))
			\\&=
			(x\residue a)
			\tensor
			x\tensor(a\residue b)
			\\&=
			(x\residue a)
			\tensor
			x\tensor(a\residue b)
			\\&=
			x\tensor
			(x\residue a)
			\tensor(a\residue b)
			\\&=
			a\tensor(a\residue b)
			\\&=
			b
		\end{align*}
	\end{proof}
	
	\begin{theorem}  [Closed Form for Local Residue]
		\thmtotoc{Closed Form for Local Residue}
		We would like to have a closed form for the local residue, and 
		this is what we provide here: 
		\[
			(a\residue_x b) = [(x\residue a)\residue b]\wedge x
		\]
	\end{theorem}
	\begin{proof}
		By definition, \(a\residue_x b\) is given by a supremum:
		\[
			\sup\setbuilder{c\leq x}{a\tensor_x c \leq b}
		\]
		\begin{prooftree}
			\AxiomC	
				{\(c \leq a\residue_xb\)}
			\UnaryInfC
				{\(a\tensor_x c \leq b\)}
			\UnaryInfC
				{\(x\tensor(x\residue a)\tensor(x\residue c)\leq b\)}
			\UnaryInfC
				{\(x\tensor(x\residue c)\tensor(x\residue a)\leq b\)}
			\UnaryInfC
				{\(c\tensor(x\residue a)\leq b\)}
			\UnaryInfC
				{\(c\leq (x\residue a)\residue b\)}
		\end{prooftree}
		And thus we have that \(a\residue_x b\leq (x\residue a)\residue b\);
		however, we have no guarantee that \((x\residue a)\residue b\) isn't
		larger than \(x\); which is a problem. We therefore consider a clamping
		of it: \([(x\residue a)\residue b]\wedge x\); for the sake of 
		cleanliness, let \(\gamma=(x\residue a)\residue b\).
		\begin{align*}
			a\tensor_x (\gamma\wedge x) 
			&=	x\tensor[x\residue(\gamma\wedge x)]\tensor(x\residue a)
			\\&=	(\gamma\wedge x)\tensor(x\residue a)
			\\&=	(x\residue a)\tensor(\gamma\wedge x)
			\\&\leq (x\residue a)\tensor \gamma
			\\&=	(x\residue a)\tensor[(x\residue a)\residue b]
			\tag{Definition}
			\\&\leq	b
		\end{align*}
		Therefore, \(\gamma\wedge x\leq a\residue_x b\)
		\[
			\gamma\wedge x \leq a\residue_x b\leq \gamma
		\]

		Since \(a\residue_x b\leq x\), we can wedge the whole line and 
		get:
		\[
			\gamma\wedge x \leq a\residue_x b\leq \gamma\wedge x
		\]
	\end{proof}

	In category theory, one often considers categories with pullbacks, 
	which are nice limit constructions; the pullback by an arrow 
	\(f:A\to B\) is a functor 
	\(\commacat {\cat C} B \to \commacat {\cat C} A\). We often call that 
	a “chage of basis” which can also be seen as a form of substitution 
	among other things. We will not delve on the topic, but what we are 
	looking for is a functor (which really just reduces to a homomorphism 
	of some description) with nice properties like “change of basis”.

	\begin{definition}[Monoidal Change of Basis]
		\deftotoc {Monoidal Change of Basis}
		If \(y\leq x\), we define the map from \(\commacat Q x\) into
		\(\commacat Q y\) by taking \(y\tensor_x -\) -- which is 
		surprisingly straighforward as far as definitions go.
	\end{definition}

	\begin{theorem}
		We must prove that it is a good function, though, of course.
		It is obviously a complete lattice morphism, since it preserves
		suprema in \(\commacat Q x\), which coincide with those of 
		\(\commacat Q y\). So it's increasing, \emph{etc}.

		We ought to show that it is also \emph{monoiodal}, that is,
		it sends tensors to tensors; in general it does not appear to
		preserve limits -- that is, send wedges to wedges.
	\end{theorem}
	\begin{proof}
		Let \(a, b \leq x\),
		\begin{align*}
			y\tensor_x(a\tensor_x b) 
			&=	 x\tensor
				(x\residue y)\tensor
				(x\residue a)\tensor
				(x\residue b)
			%
			\\&=	 y\tensor
				(x\residue a)\tensor
				(x\residue b)
		\end{align*}

		\begin{align*}
			(y\tensor_x a)\tensor_y(y\tensor_x b) 
			&=	(y\tensor_x a)
				\tensor_y
				[x\tensor(x\residue y)\tensor(x\residue b)]
			\\&=	(y\tensor_x a)
				\tensor_y
				[y\tensor(x\residue b)]
			\\&=	y\tensor
				[y\residue(y\tensor_x a)]
				\tensor
				(y\residue[y\tensor(x\residue b)])
			\\&=	[y\residue(y\tensor_x a)]
				\tensor
				 y\tensor
				(y\residue[y\tensor(x\residue b)])
			\\&=	[y\residue(y\tensor_x a)]
				\tensor
				[y\tensor(x\residue b)]
			\\&=	y\tensor 
				[y\residue(y\tensor_x a)]\tensor
				(x\residue b)
			\\&=	(y\tensor_x a)\tensor(x\residue b)
			\\&=	 y\tensor
				(x\residue y)\tensor
				(x\residue a)\tensor
				(x\residue b)
			\\&=	 y\tensor
				(x\residue a)\tensor
				(x\residue b)
		\end{align*}
		and so they are equal; therefore \(y\tensor_x-\) is a nice type
		of homomorphism, and it also satisfies some neat inequalities 
		with \(\wedge\) and \(\residue_x\) as well.
	\end{proof}
\end{document}
